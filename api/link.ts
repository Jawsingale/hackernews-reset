import { schema  } from 'nexus'

schema.objectType ({
  name: 'Link',
  description: 'Link to a useful website',
  definition(t) {
    t.model.id()
    t.model.description()
    t.model.url()
    t.model.createdAt()
    t.model.updatedAt()

  }
})

schema.extendType({
  type: 'Query',
  definition(t){
  t.crud.link()
  t.crud.links({
    ordering:true,
    filtering: {
      description: true,
      createdAt: true,
    }
  })
  }
})

schema.extendType({
  type: 'Mutation',
  definition(t){
    t.crud.createOneLink()
    t.crud.updateOneLink()
    t.crud.updateManyLink()
    t.crud.deleteOneLink()
    t.crud.deleteManyLink()
  }
})



