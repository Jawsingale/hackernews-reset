import { PrismaClient } from '@prisma/client'
import { URL  } from 'url'

const prisma = new PrismaClient()

const main = async () => {
  const links = [
    "https://prisma.io/",
    "https://nexusjs.org/",
    "https://howtographql.com"
  ]

  for(const link of links) {
    const url = new URL(`${link}`)
    let description = url.hostname.split('.')[0]
    description = description.charAt(0).toUpperCase() + description.slice(1);
    
    const currentLink = await prisma.link.create({
      data: {
        description: `${description}`,
        url: `${link}`,
      }
    })
    console.log('Current Link: ', currentLink)
  }
}

main()
.catch(e => console.error(e))
.finally(async () => {
  await prisma.disconnect()
})
