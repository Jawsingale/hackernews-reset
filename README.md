# Readme
References:
* https://www.howtographql.com/graphql-js/0-introduction/
* https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch-sql-typescript-postgres
* https://nexusjs.org/getting-started/tutorial

## Step 1 ##
```
npm install
```
Sorry about this not sure why
```
npm remove @nexus/schema graphql
```

Go to /prisma/ and make a .env file. Add this in per prisma docs:
Name your database "hackernews-nexus"
```
DATABASE_URL="postgresql://postgres:PGPASSWORDCHANGE@localhost:5432/hackernews-nexus?schema=public"
```

You now need to adjust the connection URL to point to your own database.

The format of the connection URL for your database depends on the database you use. For PostgreSQL, it looks as follows (the parts spelled all-uppercased are placeholders for your specific connection details):

```
postgresql://USER:PASSWORD@HOST:PORT/DATABASE?schema=SCHEMA
```

Here's a short explanation of each component:

* USER: The name of your database user
* PASSWORD: The password for your database user
* PORT: The port where your database server is running (typically 5432 for PostgreSQL)
* DATABASE: The name of the database
* SCHEMA: The name of the schema inside the database

If you're unsure what to provide for the schema parameter for a PostgreSQL connection URL, you can probably omit it. In that case, the default schema name public will be used.

As an example, for a PostgreSQL database hosted on Heroku, the connection URL might look similar to this:

```
DATABASE_URL="postgresql://opnmyfngbknppm:XXX@ec2-46-137-91-216.eu-west-1.compute.amazonaws.com:5432/d50rgmkqi2ipus?schema=hello-prisma"
```

When running PostgreSQL locally on Mac OS, your user and password as well as the database name typically correspond to the current user of your OS, e.g. assuming the user is called janedoe:

```
DATABASE_URL="postgresql://janedoe:janedoe@localhost:5432/janedoe?schema=hello-prisma"
```

## Step 2 ##

```
npx prisma migrate save --experimental
```
```
npx prisma migrate up --experimental
```
```
npx prisma generate
```

## Step 3 ##
Seed Database
```
npx ts-node prisma/seed.ts
```
Start Nexus
```
npx nexus dev
```

## Step 4 ##
Goto http://localhost:4000/

Try this query out
```
query {
  links{
    description
    url
  }
}
```